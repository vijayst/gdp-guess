//
//  TTSHelper.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "TTSHelper.h"

@interface TTSHelper()

@end

@implementation TTSHelper
@synthesize delegate;
@synthesize state;

-(id)init
{
    self = [super init];
    if(self) {
        voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-GB"];
        speech = [[AVSpeechSynthesizer alloc] init];
        speech.delegate = self;
    }
    return self;
}

-(void)speakText:(NSString *)text state:(NSString *)pState
{
    AVSpeechUtterance *welcome = [[AVSpeechUtterance alloc] initWithString:text];
    welcome.rate = 0.20;
    welcome.voice = voice;
    welcome.volume = 0.5;
    state = pState;
    [speech speakUtterance:welcome];
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance
{
    if([delegate respondsToSelector:@selector(speechCompleted:)] && state!=nil) {
        [delegate speechCompleted:state];
    }
}

@end
