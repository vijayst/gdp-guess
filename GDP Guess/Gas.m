//
//  Gas.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 10/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "Gas.h"
#import "DbHelper.h"

@interface Gas()
+(void)getData;
@end

@implementation Gas

@synthesize num;
@synthesize min;
@synthesize max;

static NSMutableArray *rangeArray;

+(void)getData
{
    @synchronized(self) {
        if(rangeArray==nil) {
            DbHelper *helper = [DbHelper getInstance];
            sqlite3_stmt *statement;
            [helper executeQuery:@"SELECT id, min, max from gas" statement:&statement];
            rangeArray = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                Gas *gas = [[Gas alloc] init];
                gas.num = sqlite3_column_int(statement, 0);
                gas.min = sqlite3_column_double(statement, 1);
                gas.max = sqlite3_column_double(statement, 2);
                [rangeArray addObject:gas];
            }
            sqlite3_finalize(statement);
        }
    }
}

+(Range *)getRange:(double)gdpValue
{
    Range *range = nil;
    [self getData];
    for (Gas* gdp in rangeArray) {
        double maxValue = gdp.max;
        if(gdpValue < maxValue) {
            range = [[Range alloc] initWithMinMax:gdp.min max:maxValue];
            break;
        }
    }
    return range;
}

@end
