//
//  GuessController.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 12/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CountryDataSource.h"

@protocol CloseDelegate <NSObject>
-(void)close;
@end

@interface GuessController : UIViewController
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)goHome:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property (assign, nonatomic) id delegate;
@end
