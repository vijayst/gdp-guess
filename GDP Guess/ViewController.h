//
//  ViewController.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 03/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ViewController : BaseViewController<UITableViewDelegate>
@property (strong, nonatomic) IBOutlet UIButton *gdpButton;
@property (strong, nonatomic) IBOutlet UIButton *gdpcButton;
@property (strong, nonatomic) IBOutlet UIButton *laborButton;
@property (strong, nonatomic) IBOutlet UIButton *gasButton;
@property (strong, nonatomic) IBOutlet UIButton *regionButton;
@property (strong, nonatomic) IBOutlet UIButton *capitalButton;
@property (strong, nonatomic) IBOutlet UILabel *levelLabel;
@property (strong, nonatomic) IBOutlet UILabel *attemptLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *maxLabel;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@end
