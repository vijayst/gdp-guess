//
//  CountryDataSource.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "CountryDataSource.h"
#import "Country.h"
#import "Gdp.h"
#import "GdpCapita.h"
#import "Labor.h"
#import "Gas.h"

@implementation CountryDataSource

@synthesize countryArray;

-(id)init
{
    self = [super init];
    if(self) {
        
    }
    return self;
}

-(void)loadData
{
    countryArray = [[NSMutableArray alloc] initWithArray:[Country getCountries]];
}

-(void)filterByGdp:(double)gdp
{
    Range *range = [Gdp getRange:gdp];
    for (int i=0; i<[countryArray count]; i++) {
        Country *c = [countryArray objectAtIndex:i];
        if((c.gdp<range.min) || (c.gdp>=range.max)) {
            [countryArray removeObjectAtIndex:i--];
        }
    }
}

-(void)filterByGdpCapita:(double)gdpcapita
{
    Range *range = [GdpCapita getRange:gdpcapita];
    for (int i=0; i<[countryArray count]; i++) {
        Country *c = [countryArray objectAtIndex:i];
        if((c.gdpcapita<range.min) || (c.gdpcapita>=range.max)) {
            [countryArray removeObjectAtIndex:i--];
        }
    }
}

-(void)filterByLabor:(double)labor
{
    Range *range = [Labor getRange:labor];
    for (int i=0; i<[countryArray count]; i++) {
        Country *c = [countryArray objectAtIndex:i];
        if((c.labor<range.min) || (c.labor>=range.max)) {
            [countryArray removeObjectAtIndex:i--];
        }
    }
}

-(void)filterByGas:(double)gas
{
    Range *range = [Gas getRange:gas];
    for (int i=0; i<[countryArray count]; i++) {
        Country *c = [countryArray objectAtIndex:i];
        if((c.gas<range.min) || (c.gas>=range.max)) {
            [countryArray removeObjectAtIndex:i--];
        }
    }
}


-(void)filterByRegion:(int)region
{
    for (int i=0; i<[countryArray count]; i++) {
        Country *c = [countryArray objectAtIndex:i];
        if(c.region!=region) {
            [countryArray removeObjectAtIndex:i--];
        }
    }
}

static NSString *simpleTableIdentifier = @"SimpleTableItem";

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if(cell==nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
        cell.textLabel.font = [UIFont fontWithName:@"Palatino" size:30.0F];
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }
    cell.backgroundColor = [UIColor whiteColor];
    cell.textLabel.textColor = [UIColor blackColor];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.selected = false;
    cell.tag = 0;
    
    Country *country = [countryArray objectAtIndex:[indexPath row]];
    cell.textLabel.text = country.name;
    return cell;
}

-(NSAttributedString *)getData:(NSInteger)row
{
    Country *c = [countryArray objectAtIndex:row];
    NSMutableAttributedString *data = [[NSMutableAttributedString alloc] initWithString:c.name];
    [data addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Palatino" size:30.0F]  range:NSMakeRange(0, [c.name length])];
    NSMutableParagraphStyle *paraStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopyWithZone:nil];
    paraStyle.minimumLineHeight = 46.0F;
    [data addAttribute:NSParagraphStyleAttributeName value:paraStyle range:NSMakeRange(0, [c.name length])];
    return data;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [countryArray count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

@end
