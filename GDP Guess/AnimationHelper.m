//
//  AnimationHelper.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "AnimationHelper.h"

@implementation AnimationHelper

@synthesize delegate;

-(void) zoom:(UIView *)view state:(NSString *)state
{
    view.transform = CGAffineTransformMakeScale(0.2, 0.2);
	[UIView animateWithDuration:1.5 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         view.transform = CGAffineTransformIdentity;
                     }
                     completion:^(BOOL finished){
                         if([delegate respondsToSelector:@selector(animationCompleted:)] && state!=nil)
                             [delegate animationCompleted:state];
                     }];
}

-(void) fadeIn:(UIView *)view state:(NSString *)state
{
    [view setAlpha:0.1];
	[UIView animateWithDuration:2.0 delay:0.0 options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         [view setAlpha:1];
                     }
                     completion:^(BOOL finished){
                         if([delegate respondsToSelector:@selector(animationCompleted:)] && state!=nil)
                             [delegate animationCompleted:state];
                     }];
}

@end
