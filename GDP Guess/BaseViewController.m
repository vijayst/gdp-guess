//
//  BaseViewController.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 08/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

@synthesize attempt;
@synthesize level;
@synthesize time;
@synthesize score;
@synthesize country;
@synthesize points;

@synthesize audioHelper;
@synthesize animHelper;
@synthesize ttsHelper;
@synthesize timer;
@synthesize tableSource;
@synthesize filter;
@synthesize currFormatter;
@synthesize intFormatter;
@synthesize floatFormatter;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    tapColor = [UIColor colorWithRed:232/256.0
                               green:17/256.0
                                blue:35/256.0
                               alpha:1];
    
    timeoutColor = [UIColor colorWithRed:187/256.0
                                   green:21/256.0
                                    blue:27/256.0
                                   alpha:1];
    
    panelColor = [UIColor colorWithRed:0
                                 green:25/256.0
                                  blue:144/256.0
                                 alpha:1];
    
    gdpColor = [UIColor colorWithRed:0
                               green:114/256.0
                                blue:51/256.0
                               alpha:1];
    
    geoColor = [UIColor colorWithRed:0
                               green:115/256.0
                                blue:199/256.0
                               alpha:1];
    
    audioHelper = [[AudioHelper alloc] init];
    ttsHelper = [[TTSHelper alloc] init];
    ttsHelper.delegate = self;
    animHelper = [[AnimationHelper alloc] init];
    animHelper.delegate = self;
    tableSource = [[CountryDataSource alloc] init];
    
    currFormatter = [[NSNumberFormatter alloc] init];
    [currFormatter setPositiveFormat:@"$ #,##0"];
    
    intFormatter = [[NSNumberFormatter alloc] init];
    [intFormatter setPositiveFormat:@"#,##0"];
    
    floatFormatter = [[NSNumberFormatter alloc] init];
    [floatFormatter setPositiveFormat:@"$ #,##0.00"];
}

-(void)initGame
{
    filter = 0;
    attempt = 0;
    time = 60;
    points = 250;
    level++;
    country = [Country getRandom];
    [tableSource loadData];
    [audioHelper playGameLoadSound];
}

-(bool)checkAnswer:(NSString *)answer
{
    attempt++;
    bool check = [country.name isEqualToString:answer];
    if(!check) {
        if(attempt==1)
            points -= 50;
        if(attempt==2)
            points -= 25;
        if(attempt==3)
            points = 0;
        [audioHelper playNoMatchSound];
    } else {
        [audioHelper playMatchSound];
        [timer invalidate];
        points -= time / 5;
        if(points<0)
            points = 0;
        score += points;
    }
    return check;
}

-(void)filterByGdp
{
    [tableSource filterByGdp:country.gdp];
    filter++;
    points -= 30;
}

-(void)filterByGdpCapita
{
    [tableSource filterByGdpCapita:country.gdpcapita];
    filter++;
    points -= 30;
}

-(void)filterByLabor
{
    [tableSource filterByLabor:country.labor];
    filter++;
    points -= 30;
}

-(void)filterByGas
{
    [tableSource filterByGas:country.gas];
    filter++;
    points -= 30;
}

-(void)filterByRegion
{
    [tableSource filterByRegion:country.region];
    filter++;
    points -= 30;
}

-(void)tapCapital
{
    [self.audioHelper playMatchSound];
    points -= 150;
}

-(void)animationCompleted:(NSString *)state
{
    if([state isEqualToString:@"speakGuess"]) {
        [ttsHelper speakText:@"Guess the country?" state:@"startTimer"];
    }
}

-(void)speechCompleted:(NSString *)state
{
    if([state isEqualToString:@"startTimer"])
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateTimer) userInfo:nil repeats:true];
    if([state isEqualToString:@"newGame"])
        [self initGame];
}

-(void)updateTimer
{
    time--;
}

-(NSString *)getGdpText
{
    NSString *gdpText = [currFormatter stringFromNumber:[NSNumber numberWithInt:(int)(country.gdp/1000000)]];
    return [NSString stringWithFormat:@"%@ \nMillion", gdpText];
}

-(NSString *)getGdpCapitaText
{
    return [currFormatter stringFromNumber:[NSNumber numberWithInt:(int)country.gdpcapita]];
}

-(NSString *)getLaborText
{
    return [intFormatter stringFromNumber:[NSNumber numberWithInt:(int)country.labor]];
}

-(NSString *)getGasText
{
    return [floatFormatter stringFromNumber:[NSNumber numberWithFloat:country.gas]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
