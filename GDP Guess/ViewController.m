//
//  ViewController.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 03/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
-(void)showLandscape;
-(void)showPortrait;
-(void)setLabelStyle:(UILabel *)label;
-(void)setButtonStyle:(UIButton *)button;
-(NSAttributedString *)getLabelText:(NSString *)text value:(int)value;
@end

@implementation ViewController

@synthesize gdpButton;
@synthesize gdpcButton;
@synthesize laborButton;
@synthesize gasButton;
@synthesize regionButton;
@synthesize capitalButton;
@synthesize scrollView;
@synthesize tableView;
@synthesize levelLabel;
@synthesize attemptLabel;
@synthesize timeLabel;
@synthesize scoreLabel;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    [gdpButton setTitle:@"GDP" forState:UIControlStateNormal];
    [gdpButton addTarget:self action:@selector(filterByGdp) forControlEvents:UIControlEventTouchUpInside];
    [self setButtonStyle:gdpButton];
    
    [gdpcButton setTitle:@"GDP \nPer Capita" forState:UIControlStateNormal];
    [gdpcButton addTarget:self action:@selector(filterByGdpCapita) forControlEvents:UIControlEventTouchUpInside];
    [self setButtonStyle:gdpcButton];
    
    [laborButton setTitle:@"Labor \nForce" forState:UIControlStateNormal];
    [laborButton addTarget:self action:@selector(filterByLabor) forControlEvents:UIControlEventTouchUpInside];
    [self setButtonStyle:laborButton];

    [gasButton setTitle:@"Gasoline \nPrices" forState:UIControlStateNormal];
    [gasButton addTarget:self action:@selector(filterByGas) forControlEvents:UIControlEventTouchUpInside];
    [self setButtonStyle:gasButton];

    [regionButton setTitle:@"Region" forState:UIControlStateNormal];
    [regionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [regionButton addTarget:self action:@selector(filterByRegion) forControlEvents:UIControlEventTouchUpInside];
    [self setButtonStyle:regionButton];
    
    [capitalButton setTitle:@"Capital" forState:UIControlStateNormal];
    [capitalButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [capitalButton addTarget:self action:@selector(tapCapital) forControlEvents:UIControlEventTouchUpInside];
    [self setButtonStyle:capitalButton];
    
    [tableView setDataSource:self.tableSource];
    [tableView setDelegate:self];
    [self initGame];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initGame
{
    [super initGame];
    
    [self setLabelStyle:levelLabel];
    levelLabel.attributedText = [self getLabelText:@"Level" value:self.level];
    [self setLabelStyle:attemptLabel];
    attemptLabel.attributedText = [self getLabelText:@"Attempt" value:self.attempt];
    [self setLabelStyle:timeLabel];
    timeLabel.attributedText = [self getLabelText:@"Time" value:self.time];
    [self setLabelStyle:scoreLabel];
    scoreLabel.attributedText = [self getLabelText:@"Score" value:self.score];
    
    [tableView reloadData];
    
    gdpButton.backgroundColor = gdpColor;
    gdpcButton.backgroundColor = gdpColor;
    laborButton.backgroundColor = [UIColor colorWithRed:0.5 green:187/256.0 blue:0 alpha:1];
    gasButton.backgroundColor = [UIColor colorWithRed:0.5 green:187/256.0 blue:0 alpha:1];
    regionButton.backgroundColor = geoColor;
    capitalButton.backgroundColor = geoColor;
    
    gdpButton.enabled = true;
    gdpcButton.enabled = true;
    laborButton.enabled = true;
    gasButton.enabled = true;
    regionButton.enabled = true;
    capitalButton.enabled = true;
    
    [gdpButton setTitle:[self getGdpText] forState:UIControlStateDisabled];
    [gdpcButton setTitle:[self getGdpCapitaText] forState:UIControlStateDisabled];
    [laborButton setTitle:[self getLaborText] forState:UIControlStateDisabled];
    [gasButton setTitle:[self getGasText] forState:UIControlStateDisabled];
    [capitalButton setTitle:self.country.capital forState:UIControlStateDisabled];
    [regionButton setTitle:self.country.regionName forState:UIControlStateDisabled];
    
    [self.animHelper fadeIn:gdpButton state:nil];
    [self.animHelper fadeIn:gdpcButton state:nil];
    [self.animHelper fadeIn:laborButton state:nil];
    [self.animHelper fadeIn:gasButton state:nil];
    [self.animHelper fadeIn:regionButton state:nil];
    [self.animHelper fadeIn:capitalButton state:@"speakGuess"];
}

-(void)updateTimer
{
    [super updateTimer];
    if(self.time==0)
    {
        levelLabel.backgroundColor = timeoutColor;
        timeLabel.backgroundColor = timeoutColor;
        attemptLabel.backgroundColor = timeoutColor;
        scoreLabel.backgroundColor = timeoutColor;
        [self.timer invalidate];
    }
    timeLabel.attributedText = [self getLabelText:@"Time" value:self.time];
}

-(void)filterByGdp
{
    [self.audioHelper playMatchSound];
    if(self.filter<2) {
        [super filterByGdp];
        [tableView reloadData];
    } else {
        self.points -= 20;
    }
    gdpButton.enabled = false;
    gdpButton.backgroundColor = tapColor;
}

-(void)filterByGdpCapita
{
    [self.audioHelper playMatchSound];
    if(self.filter<2) {
        [super filterByGdpCapita];
        [tableView reloadData];
    } else {
        self.points -= 20;
    }
    gdpcButton.enabled = false;
    gdpcButton.backgroundColor = tapColor;
}

-(void)filterByLabor
{
    [self.audioHelper playMatchSound];
    if(self.filter<2) {
        [super filterByLabor];
        [tableView reloadData];
    } else {
        self.points -= 10;
    }
    laborButton.enabled = false;
    laborButton.backgroundColor = tapColor;
}

-(void)filterByGas
{
    [self.audioHelper playMatchSound];
    if(self.filter<2) {
        [super filterByGas];
        [tableView reloadData];
    } else {
        self.points -= 10;
    }
    gasButton.enabled = false;
    gasButton.backgroundColor = tapColor;
}

-(void)filterByRegion
{
    [self.audioHelper playMatchSound];
    if(self.filter<2) {
        [super filterByRegion];
        [tableView reloadData];
    } else {
        self.points -= 30;
    }
    regionButton.enabled = false;
    regionButton.backgroundColor = tapColor;
}

-(void)tapCapital
{
    [super tapCapital];
    capitalButton.enabled = false;
    capitalButton.backgroundColor = tapColor;
}

- (NSIndexPath *)tableView:(UITableView *)pTableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

- (void)tableView:(UITableView *)pTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [pTableView cellForRowAtIndexPath:indexPath];
    if(cell.tag==1)
        return;
    if(![super checkAnswer:cell.textLabel.text]) {
        cell.backgroundColor = [UIColor colorWithRed:187/256.0 green:21/256.0 blue:27/256.0 alpha:1];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.selected = false;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = 1;
        attemptLabel.attributedText = [self getLabelText:@"Attempt" value:self.attempt];

    } else {
        cell.backgroundColor = [UIColor colorWithRed:0.5 green:187/256.0 blue:0 alpha:1];
        cell.textLabel.textColor = [UIColor whiteColor];
        cell.selected = false;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        scoreLabel.attributedText = [self getLabelText:@"Score" value:self.score];
        [self.ttsHelper speakText:[NSString stringWithFormat:@"You guessed it right! The country is %@", self.country.name] state:@"newGame"];
    }
}

- (NSIndexPath *)tableView:(UITableView *)tableView willDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{

}

- (CGFloat)tableView:(UITableView *)pTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    CGSize size = CGSizeMake([pTableView bounds].size.width, 100.0F);
    NSAttributedString *text = [self.tableSource getData:[indexPath row]];
    CGRect rect = [text boundingRectWithSize:size options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) context:nil];
    return rect.size.height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] init];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont systemFontOfSize:30.0F];
    label.text = @"Guess";
    label.backgroundColor = [UIColor colorWithRed:0 green:115/256.0 blue:199/256.0 alpha:1];
    label.textColor = [UIColor whiteColor];
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 70.0F;
}

- (void)viewWillLayoutSubviews
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        [self showLandscape];
    }
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [self showPortrait];
    }
}

static int pageMargin = 40;
static int xMargin = 20;
static int yMargin = 20;

-(void)showPortrait
{
    CGRect bounds = [self.view bounds];
    float controlWidth = (bounds.size.width - 2*pageMargin - 2*xMargin)/3;
    float controlHeight = (bounds.size.height - 2*pageMargin - 3*yMargin)/4;
    
    [gdpButton setFrame:CGRectMake(pageMargin, pageMargin, controlWidth, controlHeight)];
    [gdpcButton setFrame:CGRectMake(pageMargin + controlWidth + xMargin, pageMargin, controlWidth, controlHeight)];
    [laborButton setFrame:CGRectMake(pageMargin, pageMargin + controlHeight + yMargin, controlWidth, controlHeight)];
    [gasButton setFrame:CGRectMake(pageMargin + controlWidth + xMargin, pageMargin + controlHeight + yMargin, controlWidth, controlHeight)];
    [regionButton setFrame:CGRectMake(pageMargin, pageMargin + 2*(controlHeight + yMargin), controlWidth, controlHeight)];
    [capitalButton setFrame:CGRectMake(pageMargin + controlWidth + xMargin, pageMargin+2*(controlHeight+yMargin), controlWidth, controlHeight)];
    
    [scrollView setFrame:CGRectMake(pageMargin + 2*(controlWidth + xMargin), pageMargin, controlWidth, 3*controlHeight+2*xMargin)];
    
    float labelY = pageMargin + 3*(controlHeight + yMargin) + 50.0;
    float labelWidth = (bounds.size.width - 2*pageMargin - 3*xMargin)/4.0;
    float labelHeight = controlHeight - 50.0;
    [levelLabel setFrame:CGRectMake(pageMargin, labelY, labelWidth, labelHeight)];
    [attemptLabel setFrame:CGRectMake(pageMargin + labelWidth + xMargin, labelY, labelWidth, labelHeight)];
    [timeLabel setFrame:CGRectMake(pageMargin + 2*(labelWidth + xMargin), labelY, labelWidth, labelHeight)];
    [scoreLabel setFrame:CGRectMake(pageMargin + 3*(labelWidth + xMargin), labelY, labelWidth, labelHeight)];
}

-(void)showLandscape
{
    CGRect bounds = [self.view bounds];
    float controlWidth = (bounds.size.width - 2*pageMargin - 3*xMargin)/4;
    float controlHeight = (bounds.size.height -2*pageMargin-2*yMargin)/3;
    [gdpButton setFrame:CGRectMake(pageMargin, pageMargin, controlWidth, controlHeight)];
    [gdpcButton setFrame:CGRectMake(pageMargin, pageMargin + controlHeight + yMargin, controlWidth, controlHeight)];
    [laborButton setFrame:CGRectMake(pageMargin + controlWidth + xMargin, pageMargin, controlWidth, controlHeight)];
    [gasButton setFrame:CGRectMake(pageMargin + controlWidth + xMargin, pageMargin + controlHeight + yMargin, controlWidth, controlHeight)];
    [regionButton setFrame:CGRectMake(pageMargin + 2*(controlWidth + xMargin), pageMargin, controlWidth, controlHeight)];
    [capitalButton setFrame:CGRectMake(pageMargin + 2*(controlWidth + xMargin), pageMargin + controlHeight+yMargin, controlWidth, controlHeight)];
    
    [scrollView setFrame:CGRectMake(pageMargin + 3*(controlWidth + xMargin), pageMargin, controlWidth, 2*controlHeight+xMargin)];
    
    float labelX = 128.0;
    float labelY = pageMargin + 2*(controlHeight + yMargin) + 50.0;
    float labelWidth = (bounds.size.width - 2*pageMargin - 3*xMargin - 256)/4.0;
    float labelHeight = controlHeight - 50.0;
    [levelLabel setFrame:CGRectMake(labelX, labelY, labelWidth, labelHeight)];
    [attemptLabel setFrame:CGRectMake(labelX + labelWidth + xMargin, labelY, labelWidth, labelHeight)];
    [timeLabel setFrame:CGRectMake(labelX + 2*(labelWidth + xMargin), labelY, labelWidth, labelHeight)];
    [scoreLabel setFrame:CGRectMake(labelX + 3*(labelWidth + xMargin), labelY, labelWidth, labelHeight)];
}

-(void)setLabelStyle:(UILabel *)label
{
    label.numberOfLines = 2;
    label.textColor = [UIColor whiteColor];
    [label setTextAlignment:NSTextAlignmentCenter];
    label.backgroundColor = panelColor;
}

-(void)setButtonStyle:(UIButton *)button
{
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateDisabled];
    button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleLabel.font = [UIFont systemFontOfSize:30.0F];
}

-(NSAttributedString *)getLabelText:(NSString *)text value:(int)value
{
    NSMutableAttributedString *labelText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%d", text, value]];
    NSUInteger len1 = [text length];
    NSUInteger len2 = [labelText length] - (len1 + 1);
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:20.0F] range:NSMakeRange(0,len1)];
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.paragraphSpacing = 25;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    [labelText addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0,len1)];
    [labelText addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:50.0F] range:NSMakeRange(len1+1,len2)];
    return labelText;
}

@end
