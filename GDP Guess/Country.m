//
//  Country.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "Country.h"
#import "DbHelper.h"
#import "Region.h"

@implementation Country

@synthesize shortName;
@synthesize name;
@synthesize capital;
@synthesize gdp;
@synthesize gdpcapita;
@synthesize labor;
@synthesize gas;
@synthesize region;
@synthesize regionName;

static NSMutableArray *countries;

+(NSMutableArray *)getCountries
{
    @synchronized(self) {
        if(countries==nil) {
            DbHelper *helper = [DbHelper getInstance];
            sqlite3_stmt *statement;
            [helper executeQuery:@"SELECT id, name, region, gdp, gdpcapita, labor, gas, capital from country" statement:&statement];
            countries = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                Country *country = [[Country alloc] init];
                country.shortName = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
                country.name = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                country.region = sqlite3_column_int(statement, 2);
                country.gdp = sqlite3_column_double(statement, 3);
                country.gdpcapita = sqlite3_column_double(statement, 4);
                country.labor = sqlite3_column_int(statement, 5);
                country.gas = sqlite3_column_double(statement, 6);
                country.capital = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 7)];
                [countries addObject:country];
            }
            sqlite3_finalize(statement);
        }
    }
    return countries;
}

+(Country *)getRandom
{
    NSMutableArray *countryArray = [Country getCountries];
    NSUInteger count = [countryArray count];
    int random = arc4random() % count;
    Country *country =[countryArray objectAtIndex:random];
    if(country.regionName==nil) {
        country.regionName = [Region getName:country.region];
    }
    return country;
}

@end
