//
//  TTSHelper.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVSpeechSynthesis.h>

@protocol SpeechCompletedDelegate
@optional
-(void)speechCompleted:(NSString *)state;
@end

@interface TTSHelper : NSObject<AVSpeechSynthesizerDelegate>
{
    AVSpeechSynthesisVoice *voice;
    AVSpeechSynthesizer *speech;
}
-(void)speakText:(NSString *)text state:(NSString *)state;
@property (assign, nonatomic) id delegate;
@property (assign, nonatomic) NSString *state;
@end
