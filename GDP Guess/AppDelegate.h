//
//  AppDelegate.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 03/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
