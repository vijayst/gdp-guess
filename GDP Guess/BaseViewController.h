//
//  BaseViewController.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 08/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AudioHelper.h"
#import "TTSHelper.h"
#import "AnimationHelper.h"
#import "CountryDataSource.h"
#import "Country.h"

@interface BaseViewController : UIViewController<AnimationCompletedDelegate, SpeechCompletedDelegate, UITableViewDelegate>
{
    UIColor *tapColor;
    UIColor *timeoutColor;
    UIColor *panelColor;
    UIColor *gdpColor;
    UIColor *geoColor;
}
@property int level;
@property int attempt;
@property int time;
@property int score;
@property int filter;
@property int points;

@property (strong, nonatomic) Country *country;
@property (strong, nonatomic) AudioHelper *audioHelper;
@property (strong, nonatomic) TTSHelper *ttsHelper;
@property (strong, nonatomic) AnimationHelper *animHelper;
@property (strong, nonatomic) NSTimer *timer;
@property (strong, nonatomic) CountryDataSource *tableSource;
@property (strong, nonatomic) NSNumberFormatter *currFormatter;
@property (strong, nonatomic) NSNumberFormatter *intFormatter;
@property (strong, nonatomic) NSNumberFormatter *floatFormatter;

-(void)initGame;
-(void)updateTimer;
-(void)filterByGdp;
-(void)filterByGdpCapita;
-(void)filterByLabor;
-(void)filterByGas;
-(void)filterByRegion;
-(void)tapCapital;
-(bool)checkAnswer:(NSString *)answer;

-(NSString *)getGdpText;
-(NSString *)getGdpCapitaText;
-(NSString *)getLaborText;
-(NSString *)getGasText;
@end
