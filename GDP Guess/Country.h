//
//  Country.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Country : NSObject
@property (copy, nonatomic) NSString *shortName;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *capital;
@property int region;
@property (copy, nonatomic) NSString *regionName;
@property double gdp;
@property double gdpcapita;
@property int labor;
@property double gas;

+(NSMutableArray *)getCountries;
+(Country *)getRandom;
@end
