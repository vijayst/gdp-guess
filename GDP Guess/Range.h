//
//  Range.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Range : NSObject
@property double min;
@property double max;
-(id)initWithMinMax:(double)min max:(double)max;
@end
