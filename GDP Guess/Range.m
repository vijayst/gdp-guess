//
//  Range.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "Range.h"

@implementation Range
@synthesize min;
@synthesize max;

-(id)initWithMinMax:(double)minValue max:(double)maxValue
{
    self = [super init];
    if(self)
    {
        min = minValue;
        max = maxValue;
    }
    return self;
}

@end
