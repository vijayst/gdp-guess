//
//  AnimationHelper.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AnimationCompletedDelegate
@optional
-(void)animationCompleted:(NSString *)state;
@end

@interface AnimationHelper : NSObject
-(void) zoom:(UIView *)view state:(NSString *)state;
-(void) fadeIn:(UIView *)view state:(NSString *)state;
@property (assign, nonatomic) id delegate;
@end
