//
//  AudioHelper.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "AudioHelper.h"

@interface AudioHelper()
-(void)play:(NSURL *)url;
@end

@implementation AudioHelper 

@synthesize player;

-(id)init
{
    self = [super init];
    if(self!=nil)
    {
        
    }
    return self;
}

+(void)createAudioSession
{
    NSError *error;
    
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    BOOL success = [[AVAudioSession sharedInstance] setActive:YES error: &error];
    if(!success)
        NSLog(@"The audio session activation error is %@" , error.description);
    else
        NSLog(@"The audio session is activated");
}

-(void)playAppLoadSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"appload" ofType:@"wav" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)playGameLoadSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"gameload" ofType:@"wav" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)playMatchSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"match" ofType:@"wav" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)playNoMatchSound
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"nomatch" ofType:@"wav" inDirectory:@""];
    NSURL *url = [[NSURL alloc] initFileURLWithPath: path];
    [self play:url];
}

-(void)play:(NSURL *)url
{
    NSError *error;
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error: &error];
    player.volume = 0.4;
    if(error)
        NSLog(@"Error in loading - %@", error.description);
    player.delegate = self;
    [player play];
}


@end
