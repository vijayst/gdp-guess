//
//  Labor.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 10/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "Labor.h"
#import "DbHelper.h"

@interface Labor()
+(void)getData;
@end

@implementation Labor

@synthesize num;
@synthesize min;
@synthesize max;

static NSMutableArray *rangeArray;

+(void)getData
{
    @synchronized(self) {
        if(rangeArray==nil) {
            DbHelper *helper = [DbHelper getInstance];
            sqlite3_stmt *statement;
            [helper executeQuery:@"SELECT id, min, max from labor" statement:&statement];
            rangeArray = [[NSMutableArray alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                Labor *gdp = [[Labor alloc] init];
                gdp.num = sqlite3_column_int(statement, 0);
                gdp.min = sqlite3_column_int(statement, 1);
                gdp.max = sqlite3_column_int(statement, 2);
                [rangeArray addObject:gdp];
            }
            sqlite3_finalize(statement);
        }
    }
}

+(Range *)getRange:(double)gdpValue
{
    Range *range = nil;
    [self getData];
    for (Labor* gdp in rangeArray) {
        double maxValue = gdp.max*1000000.0;
        if(gdpValue < maxValue) {
            range = [[Range alloc] initWithMinMax:gdp.min*1000000.0 max:maxValue];
            break;
        }
    }
    return range;
}

@end
