//
//  Region.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Region : NSObject
@property int num;
@property NSString *name;

+(NSMutableDictionary *)getRegions;
+(NSString *)getName:(int)num;
@end
