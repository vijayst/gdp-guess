//
//  Labor.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 10/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Range.h"

@interface Labor : NSObject
@property int num;
@property int min;
@property int max;

+(Range *)getRange:(double)gdp;
@end
