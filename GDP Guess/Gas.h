//
//  Gas.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 10/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Range.h"

@interface Gas : NSObject
@property int num;
@property double min;
@property double max;
+(Range *)getRange:(double)gdp;
@end
