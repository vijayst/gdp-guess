//
//  Region.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "Region.h"
#import "DbHelper.h"

@implementation Region
@synthesize num;
@synthesize name;

static NSMutableDictionary *regions;

+(NSMutableDictionary *)getRegions
{
    @synchronized(self) {
        if(regions==nil) {
            DbHelper *helper = [DbHelper getInstance];
            sqlite3_stmt *statement;
            [helper executeQuery:@"SELECT id, name from region" statement:&statement];
            regions = [[NSMutableDictionary alloc] init];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                Region *region = [[Region alloc] init];
                region.name = [[NSString alloc] initWithUTF8String:(char *)sqlite3_column_text(statement, 1)];
                region.num = sqlite3_column_int(statement, 0);
                [regions setObject:region.name forKey:[NSNumber numberWithInt:region.num]];
            }
            sqlite3_finalize(statement);
        }
    }
    return regions;
}

+(NSString *)getName:(int)num
{
    NSDictionary *regionDic = [Region getRegions];
    return regionDic[[NSNumber numberWithInt:num]];
}

@end
