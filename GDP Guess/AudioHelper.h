//
//  AudioHelper.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVAudioSession.h>

@interface AudioHelper : NSObject<AVAudioPlayerDelegate>
-(void)playAppLoadSound;
-(void)playGameLoadSound;
-(void)playMatchSound;
-(void)playNoMatchSound;

+(void)createAudioSession;
@property (nonatomic, strong) AVAudioPlayer *player;
@end
