//
//  GuessController.m
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 12/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import "GuessController.h"
#import "iPhoneViewcontroller.h"

@implementation GuessController

@synthesize scrollView;
@synthesize tableView;
@synthesize backButton;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (IBAction)goHome:(id)sender {
    if([delegate respondsToSelector:@selector(close)]) {
        [delegate close];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [backButton setTitle:@"Back" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    backButton.backgroundColor = [UIColor colorWithRed:0 green:115/256.0 blue:199/256.0 alpha:1];
    
    iPhoneViewController *iPhoneController = (iPhoneViewController *)self.presentingViewController;
    [tableView setDataSource:iPhoneController.tableSource];
    [tableView setDelegate:iPhoneController];
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

- (void)viewWillLayoutSubviews
{
    if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation))
    {
        [self showLandscape];
    }
    if (UIInterfaceOrientationIsPortrait(self.interfaceOrientation))
    {
        [self showPortrait];
    }
}


-(void)showPortrait
{
    CGRect bounds = [self.view bounds];
    [backButton setFrame:CGRectMake(30.0, 30.0, 100.0, 30.0)];
    [scrollView setFrame:CGRectMake(30.0, 60.0, bounds.size.width-60.0, bounds.size.height-70.0)];
}

-(void)showLandscape
{
    CGRect bounds = [self.view bounds];
    [backButton setFrame:CGRectMake(30.0, 30.0, 100.0, 30.0)];
    [scrollView setFrame:CGRectMake(30.0, 60.0, bounds.size.width-60.0, bounds.size.height-70.0)];
}

@end
