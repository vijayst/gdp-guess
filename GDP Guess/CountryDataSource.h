//
//  CountryDataSource.h
//  GDP Guess
//
//  Created by Vijay Thirugnanam on 09/02/14.
//  Copyright (c) 2014 VijayT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryDataSource : NSObject<UITableViewDataSource>
@property (copy, nonatomic) NSMutableArray *countryArray;
-(void)filterByGdp:(double)gdp;
-(void)filterByGdpCapita:(double)gdpcapita;
-(void)filterByLabor:(double)labor;
-(void)filterByGas:(double)gas;
-(void)filterByRegion:(int)region;
-(void)loadData;
-(NSAttributedString *)getData:(NSInteger)row;
@end
